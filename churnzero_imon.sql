CREATE TYPE pmh_platform AS ENUM ('SPARK', 'EDGE');
CREATE TYPE pmh_status AS ENUM ('NEW', 'SENDING', 'COMPLETED');

-- DROP TABLE tracked_events;
CREATE TABLE tracked_events 
(
    id BIGSERIAL PRIMARY KEY,
    platform PMH_PLATFORM DEFAULT 'SPARK',
    status PMH_STATUS DEFAULT 'NEW',
    client_id BIGINT,
    client_sfid TEXT,
    client_name TEXT,
    contact_id BIGINT,
    contact_email TEXT,
    event_name TEXT,
    event_rid BIGINT,
    event_ts TIMESTAMP DEFAULT NOW()
);