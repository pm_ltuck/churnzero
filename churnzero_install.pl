#!/usr/bin/perl

###############################################################################
# Script:  churnzero_install.pl
# Author:  Larry Tuck
# Created: 2018-07-26
# Purpose: To install all components of the churnzero feature
###############################################################################

use strict;
use warnings;
use POSIX 'strftime';
use File::Copy;
use File::Path qw( make_path );

my $pg_lib = '/usr/local/pg94/lib/';
my $pg_xtens = '/usr/local/pg94/share/postgresql/extension/';
my $pg_build = '/home/build/src/postgresql-9.4.7/contrib/dblink/';
my $pg_dblnk = 'dblink.control';

# First we need to ensure that the library for dblink exists...
if (! -e $pg_lib . 'dblink.so') {
   copy($pg_build . 'dblink.so', $pg_lib) or die "Copy to lib folder failed! $!\n";
}

# Next we need to ensure that the dblink package for postgres is available...
if (! -e $pg_xtens . $pg_dblnk) {
   copy($pg_build . 'dblink--1.0--1.1.sql', $pg_xtens) or die "Copy to extensions folder failed! $!\n";
   copy($pg_build . 'dblink--1.1.sql', $pg_xtens) or die "Copy to extensions folder failed! $!\n";
   copy($pg_build . 'dblink.control', $pg_xtens) or die "Copy to extensions folder failed! $!\n";
   copy($pg_build . 'dblink--unpackaged--1.0.sql', $pg_xtens) or die "Copy to extensions folder failed! $!\n";
}

# Now that we have the library and extension in place we need to activate it...
my $pg_psql = '/usr/local/pg94/bin/psql -t -A ';
`$pg_psql . ' -c "CREATE EXTENSION dblink;" playmaker'` or die "Unable to create extension! $!\n";

# Make sure the log directory exists!
if (! -d '/var/log/perl') {
   make_path '/var/log/perl' or die 'Failed to create log directory! $!\n';
}

# Cool... we have the perquisite taken care of... Now run a bunch of stuff... 
# Since it is already written in files we just need to run those files.

# First file to run is the creation of the table in the imon db.
`$pg_psql . ' -f "/usr/local/pm_conf/scripts/churnzero_imon.sql" imon'` or die "Unable to create the table in the imon database! $!\n";
unlink '/usr/local/pm_conf/scripts/churnzero_imon.sql' or warn "Unable to remove churnzero_imon.sql! $!\n";

# Now create the event functions and triggers... including ensuring their existence for when an account is created
`$pg_psql . ' -f "/usr/local/pm_conf/scripts/churnzero.sql" playmaker'` or die "Unable to create the event functions and triggers in playmaker! $!\n";
unlink '/usr/local/pm_conf/scripts/churnzero.sql' or warn "Unable to remove churnzero.sql! $!\n";

exit 0;