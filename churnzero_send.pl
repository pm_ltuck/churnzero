#!/usr/bin/perl

###############################################################################
# Script:  churnzero_send.pl
# Author:  Larry Tuck
# Created: 2018-07-20
# Purpose: To send all events that have not already been sent. Will update the
#          status field. Sending means the event is in the process of being sent
#          and completed means the event has finished the process of being sent.
###############################################################################
# Sample CURL call:
# https://analytics.churnzero.net/i?appKey=123&accountExternalId=456&contactExternalId=789&action=trackEvent&eventName=Email+Sent

use strict;
use warnings;
use POSIX 'strftime';

my $perl_log  = '/var/log/perl/churnzero.log';
my $api_key   = 'PT7jgA3Ee79JwHnjctkuqvvIj4FnRfL1TaWuY08AqAc';
my $pg_base   = '/usr/local/pg94';


### DO NOT CHANGE ANYTHING BELOW THIS LINE ###

open LOG, ">>$perl_log" or die "Unable to open log! $!\n";
print LOG "Send started: " . (strftime "%F_%T", localtime) . "\n";

if (`ps -aef | grep -v grep churnzero_send`) {
   print 'Last job still runnig!\n';
   print LOG "Send Compelted: " . (strftime "%F_%T", localtime) . "\n";
   close LOG;
   exit 0;
}


my $sql_send  = <<'END_SQL_UPDSEND';
SELECT id, client_sfid, contact_email, event_name, REPLACE(TO_CHAR(event_ts, 'yyyy-mm-ddT HH24:MIZ'), ' ', '') AS event_ts
  FROM tracked_events
 WHERE status = 'SENDING';
END_SQL_UPDSEND

my $sql_upd_send  = <<'END_SQL_SELECT';
UPDATE tracked_events
   SET status = 'SENDING'
 WHERE status = 'NEW'
   AND client_sfid IS NOT NULL;
END_SQL_SELECT

my $sql_upd_comp  = <<'END_SQL_UPDCOMP';
UPDATE tracked_events
   SET status = 'COMPLETED'
 WHERE status = 'SENDING'
   AND id = 
END_SQL_UPDCOMP
chomp $sql_upd_comp;

# Setup the commands...
my $sql_cmd1 = $pg_base . "/bin/psql -U postgres -t -A -c \"" . $sql_upd_send . "\" imon;";
my $sql_cmd2 = $pg_base . "/bin/psql -U postgres -t -A -c \"" . $sql_send . "\" imon;";

# init variable to track if an error occurred
my $err = 0;

# First we set all new commands to sending... This way if we get a new events 
# they will not confuse the script...
`$sql_cmd1`;
if ($? > 0) {
   print LOG "ERROR: Unable to set event records to sending! $!\n";
   $err = 1;
}

# declare loop variables in a local context.
my @cols;
my $base_uri = 'https://analytics.churnzero.net/i?appKey='.$api_key;
my $base_metric = 'https://ea882d44-def6-44ef-ab80-c9010871db49@www.hostedgraphite.com/api/v1/sink --data-binary ';
my $uri = '';
my $sql_cmd3 = '';
my $id = 0;
my $client_id = '';
my $customer_id = '';
my $event_name = '';
my $event_ts = '';
my $results = '';

# Now we can retrieve all records in the state of sending.
# Note this means events that failed after they were set to sending will be grabbed again!
my $output = `$sql_cmd2`;
if ($? > 0) {
   print LOG "ERROR: Unable to get event records to send! $!\n";
   $err = 2;
}

my @lines = split(/[\r\n]+/, $output);
my $qued_events = @lines;
my $sent_events = 0;

if ($qued_events > 0 && $err == 0) {
   print LOG "Events to Send: " . $qued_events . "\n";

   foreach my $line (@lines) {
      @cols = split /\|/, $line;
      $id = $cols[0];
      $client_id = $cols[1];
      $customer_id = $cols[3];
      $event_name = $cols[4];
      $event_ts = $cols[5];

      $uri = $base_uri."&accountExternalId=".$client_id."&contactExternalId=".$customer_id."&action=trackEvent&eventName=".$event_name."&eventDate=".$event_ts;
      $results = `curl -s '$uri'`;
      if ($? > 0) {
         print LOG "Successfully sent $sent_events records...\n";
         print LOG "ERROR: Failed to send event record! $!\n";
         $err = 4;
         last;
      }

      $sql_cmd3 = $pg_base . "/bin/psql -U postgres -t -A -c \"" . $sql_upd_comp . $id . "\" imon;";
      `$sql_cmd3`;
      if ($? > 0) {
         print LOG "Successfully sent $sent_events records...\n";
         print LOG "ERROR: Unable to update event record to completed! $!\n";
         $err=5;
         last;
      }
      
      $sent_events++;
   }

   print LOG "Events Sent: " . $sent_events . "\n";
} elsif ($qued_events == 0 && $err == 0) {
   print LOG "Nothing to do!\n";
}

my $metric_uri = $base_metric . "\"SPARK.QA.Integration.ChurnZero.Tracked_Events.Qued $qued_events\"";
`curl -s $metric_uri`;
if ($? > 0) {
   print LOG "Unable to send metric data! $!\n";
   $err = 6;
}

$metric_uri = $base_metric . "\"SPARK.QA.Integration.ChurnZero.Tracked_Events.Sent $sent_events\"";
`curl -s $metric_uri`;
if ($? > 0) {
   print LOG "Unable to send metric data! $!\n";
   $err = 6;
}

print LOG "Send Compelted: " . (strftime "%F_%T", localtime) . "\n";
close LOG;

exit $err;