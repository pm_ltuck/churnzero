-- DROP FUNCTION public.add_tracked_event(client_id BIGINT, contact_id BIGINT, event_record_id BIGINT, event_name TEXT);
CREATE OR REPLACE FUNCTION public.add_tracked_event(client_id BIGINT, contact_id BIGINT, event_record_id BIGINT, event_name TEXT) 
RETURNS BOOLEAN AS $$
DECLARE
   client_sfid TEXT;
   client_name TEXT;
   contact_email TEXT;
   event_id BIGINT;
   cmd_ins TEXT;
   cmd_rslt TEXT;
   cmd_cntx TEXT;
BEGIN
   -- Setup the connnection to the imon database.
   SELECT dblink_connect('imon_con', 'dbname=imon') INTO cmd_rslt;
   
   -- Set the clontact email from the contact_id
   IF contact_id > 0 THEN 
      SELECT email INTO contact_email FROM public.users WHERE id = contact_id;
   ELSE
      SELECT id, email INTO contact_id, contact_email FROM public.users WHERE accounts_id = client_id AND email ILIKE '%@pmcrm.net' LIMIT 1;
   END IF;
   
   -- If we do not have the client id then we need to get it based on the contact_id (users_id)
   IF client_id = 0::BIGINT THEN 
      SELECT accounts_id INTO client_id FROM users WHERE id = contact_id;
   END IF;
   
   -- WE need to get the Sales Force Id from the imon database. Also, we will get the salesforce name as well.
   SELECT sf_id, name INTO client_sfid, client_name FROM public.dblink('imon_con', 'select sf_id, name FROM public.company WHERE version = 4 AND accounts_id = ' || client_id) AS imon (sf_id TEXT, name TEXT);

   -- Assemble the command to execute on the imon database.
   cmd_ins := FORMAT('INSERT INTO public.tracked_events ("client_id", "client_sfid", "client_name", "contact_id", "contact_email", "event_name", "event_rid") 
                                                  VALUES ( %s,          ''%s'',        ''%s'',        %s,           ''%s'',          ''%s'',       %s)', 
                                                           client_id,   client_sfid,   client_name,   contact_id,   contact_email,   event_name,   event_record_id
                    );

   -- Execute the command on the imon connection.
   SELECT dblink_exec ('imon_con', cmd_ins) INTO cmd_rslt;
   
   -- Release the connection, we do not want to leak any resources...
   SELECT dblink_disconnect('imon_con') INTO cmd_rslt;
   
   RETURN true;

-- Make sure to release the connection... Even on an exception!        
EXCEPTION WHEN others THEN
   GET STACKED DIAGNOSTICS cmd_cntx = PG_EXCEPTION_CONTEXT;
   RAISE NOTICE 'STATE: % MESSAGE: % CMD: % RESULT: % CONTEXT %', SQLSTATE, SQLERRM, cmd_ins, cmd_rslt, cmd_cntx;
   SELECT dblink_disconnect('imon_con') INTO cmd_rslt;
   RETURN false;

END;
$$  LANGUAGE plpgsql; 

-- SELECT public.add_tracked_event(0, 702283, 2503, 'test_account_add');


-- Public tables and events...
CREATE OR REPLACE FUNCTION public.user_login_history_event()
RETURNS TRIGGER AS $$

BEGIN
   IF (OLD.last_web_login IS NOT NULL AND NEW .last_web_login IS NOT NULL AND OLD.last_web_login != NEW.last_web_login) OR (OLD.last_web_login IS NULL AND NEW.last_web_login IS NOT NULL) THEN
      PERFORM public.add_tracked_event(0, NEW.users_id, NEW.users_id, 'SPARK_Web_Login');
   END IF;

   IF (OLD.last_mobile_login IS NOT NULL AND NEW .last_mobile_login IS NOT NULL AND OLD.last_mobile_login != NEW.last_mobile_login) OR (OLD.last_mobile_login IS NULL AND NEW.last_mobile_login IS NOT NULL) THEN
      PERFORM public.add_tracked_event(0, NEW.users_id, NEW.users_id, 'SPARK_Mobile_Login');
   END IF;
   
   RETURN NEW;
END;
$$ LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION public.user_login_history_new_event()
RETURNS TRIGGER AS $$

BEGIN
   IF NEW.last_web_login IS NOT NULL THEN
      PERFORM public.add_tracked_event(0, NEW.users_id, NEW.users_id, 'SPARK_Web_Login');
   END IF;

   IF NEW.last_mobile_login IS NOT NULL THEN
      PERFORM public.add_tracked_event(0, NEW.users_id, NEW.users_id, 'SPARK_Mobile_Login');
   END IF;
   
   RETURN NEW;
END;
$$ LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION public.users_event()
RETURNS TRIGGER AS $$
BEGIN
   IF NEW.status = 2 THEN
      PERFORM public.add_tracked_event(NEW.accounts_id, NEW.id, NEW.id, 'SPARK_User_Added');
   END IF;
   
   IF NEW.status = 1 THEN
      PERFORM public.add_tracked_event(NEW.accounts_id, NEW.id, NEW.id, 'SPARK_User_Disabled');
   END IF;

   IF NEW.status = 0 THEN
      PERFORM public.add_tracked_event(NEW.accounts_id, NEW.id, NEW.id, 'SPARK_User_Deleted');
   END IF;
   
   RETURN NEW;
END;
$$ LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION public.users_registry_event()
RETURNS TRIGGER AS $$
BEGIN
   IF NEW.name = 'login-access' AND TO_DATE(NEW.value, 'MM/DD/YYYY') > CURRENT_DATE THEN
      PERFORM public.add_tracked_event(0, NEW.users_id, NEW.id, 'SPARK_Granted_Support_Login_Access');
   END IF;
   
   RETURN NEW;
END;
$$ LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION public.scheduled_reports_event()
RETURNS TRIGGER AS $$
BEGIN
   PERFORM public.add_tracked_event(0, NEW.users_id, NEW.id, 'SPARK_Report_Scheduled');
   
   RETURN NEW;
END;
$$ LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION public.report_ran_event()
RETURNS TRIGGER AS $$
BEGIN
   PERFORM public.add_tracked_event(0, NEW.users_id, NEW.id, 'SPARK_Report_Ran');
   
   RETURN NEW;
END;
$$ LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER user_login_history_updtr 
AFTER UPDATE ON public.users_login_history
FOR EACH STATEMENT
    EXECUTE PROCEDURE user_login_history_event();

CREATE TRIGGER user_login_history_instr 
AFTER INSERT ON public.users_login_history
FOR EACH STATEMENT
    EXECUTE PROCEDURE user_login_history_new_event();


CREATE TRIGGER users_updtr 
AFTER UPDATE OF status ON public.users
FOR EACH ROW
    WHEN (OLD.status != NEW.status)
    EXECUTE PROCEDURE users_event();

CREATE TRIGGER users_instr 
AFTER INSERT ON public.users
FOR EACH ROW
    EXECUTE PROCEDURE users_event();


CREATE TRIGGER user_registry_updtr 
AFTER UPDATE ON public.user_registry
FOR EACH ROW
    WHEN (OLD.* IS DISTINCT FROM NEW.*)
    EXECUTE PROCEDURE users_registry_event();

CREATE TRIGGER user_registry_instr 
AFTER INSERT ON public.user_registry
FOR EACH ROW
    EXECUTE PROCEDURE users_registry_event();


CREATE TRIGGER scheduled_reports_instr 
AFTER INSERT ON public.scheduled_reports
FOR EACH ROW
    EXECUTE PROCEDURE scheduled_reports_event();


CREATE TRIGGER report_ran_instr 
AFTER INSERT ON public.scheduled_reports_log
FOR EACH ROW
    EXECUTE PROCEDURE report_ran_event();
    
CREATE TRIGGER report_ran_instr 
AFTER INSERT ON public.static_reports_ran
FOR EACH ROW
    EXECUTE PROCEDURE report_ran_event();


-- Account specific tables and events
DO $$
DECLARE
   r record;
   cmd text;
BEGIN
   FOR r IN SELECT id, db_id FROM public.accounts WHERE status = 2
   LOOP
      RAISE NOTICE 'Processing Account Id: %', r.id;
   
      cmd := FORMAT('CREATE OR REPLACE FUNCTION %1$s.change_log_event() RETURNS TRIGGER AS $TRG1$
                     BEGIN
                        SET search_path TO %1$s, public, targetwatch;

                        -- Add actions
                        IF NEW.action = 1 THEN
                           IF NEW.object_list_id = objectid(''expenses'') THEN
                              PERFORM public.add_tracked_event(%2$s, NEW.users_id, NEW.action_id, ''SPARK_Expense_Added'');
                           ELSIF NEW.object_list_id = objectid(''events'') THEN
                              PERFORM public.add_tracked_event(%2$s, NEW.users_id, NEW.action_id, ''SPARK_Event_Created'');
                           ELSIF NEW.object_list_id = objectid(''events_series'') THEN
                              PERFORM public.add_tracked_event(%2$s, NEW.users_id, NEW.action_id, ''SPARK_Recurring_Event_Series_Created'');
                           END IF;
                        -- Update Actions
                        ELSIF NEW.action = 2 THEN
                           IF NEW.object_list_id = objectid(''events'') AND NEW.notes = ''Set complete: Completed'' THEN
                              PERFORM public.add_tracked_event(%2$s, NEW.users_id, NEW.action_id, ''SPARK_Event_Completed'');
                           END IF;
                        -- Delete Actions
                        ELSIF NEW.action = 3 THEN
                           IF NEW.object_list_id = objectid(''territory'') THEN
                              PERFORM public.add_tracked_event(%2$s, NEW.users_id, NEW.action_id, ''SPARK_Sales_Territory_Removed'');
                           END IF;
                        END IF;
                        
                        RETURN NEW;
                     END;
                     $TRG1$ LANGUAGE plpgsql VOLATILE;', r.db_id, r.id, '%coachs_list%', '%"coachs_list_id":null%');
      RAISE NOTICE '\tAdding Change Log Events...';
      EXECUTE cmd;

      cmd := FORMAT('CREATE OR REPLACE FUNCTION %1$s.applog_event() RETURNS TRIGGER AS $TRG2$
                     DECLARE
                        ids BIGINT[];
                     BEGIN
                        SET search_path TO %1$s, public, targetwatch;
                        
                        -- View actions
                        IF NEW.uri ILIKE %3$L THEN
                           PERFORM public.add_tracked_event(%2$s, NEW.users_id, NEW.id, ''SPARK_Request_for_Account'');
                        ELSIF NEW.uri ILIKE %4$L THEN
                           PERFORM public.add_tracked_event(%2$s, NEW.users_id, NEW.id, ''SPARK_Request_for_Contact'');
                        ELSIF NEW.uri ILIKE %5$L THEN
                           PERFORM public.add_tracked_event(%2$s, NEW.users_id, NEW.id, ''SPARK_Request_for_Referral'');
                        ELSIF NEW.uri ILIKE %6$L THEN
                           SELECT STRING_TO_ARRAY(REPLACE(SUBSTRING(uri, POSITION(''ids'' in uri)+4, POSITION(''redirect'' in uri) - (POSITION(''ids'' in uri)+5)), ''/'',''''), '','')::BIGINT[] INTO ids  FROM %1$s.applog WHERE uri ILIKE %6$L;
                           IF EXISTS (SELECT 1 FROM %1$s.merged_records WHERE from_id = ANY(ARRAY[ids])) THEN
                              PERFORM public.add_tracked_event(%2$s, NEW.users_id, NEW.id, ''SPARK_Merge_Completed'');
                           END IF;
                        ELSIF NEW.uri ILIKE %7$L THEN
                           IF NEW.uri ILIKE %8$L AND NEW.uri NOT ILIKE %9$L THEN
                              PERFORM public.add_tracked_event(%2$s, NEW.users_id, NEW.id, ''SPARK_TargetWatch_Export'');
                           END IF;
                        END IF;
                        
                        RETURN NEW;
                     END;
                     $TRG2$ LANGUAGE plpgsql VOLATILE;', r.db_id, r.id, '/s3/contact_accounts%', '/s3/contacts%', '/s3/referrals%', '%merge%redirect%', '%export%', '/ajax/targetwatch%', '%delete%');
      RAISE NOTICE '\tAdding App Log Events...';
      EXECUTE cmd;


      cmd := FORMAT('CREATE OR REPLACE FUNCTION %1$s.tw_target_event() RETURNS TRIGGER AS $TRG3$
                     BEGIN
                        PERFORM public.add_tracked_event(%2$s, NEW.users_id, NEW.id, ''SPARK_Target_Created'');
                        
                        RETURN NEW;
                     END;
                     $TRG3$ LANGUAGE plpgsql VOLATILE;

                     CREATE OR REPLACE FUNCTION %1$s.object_list_event() RETURNS TRIGGER AS $TRG4$
                     BEGIN
                        PERFORM public.add_tracked_event(%2$s, 0, NEW.id, ''SPARK_Custom_Object_Created'');
                        
                        RETURN NEW;
                     END;
                     $TRG4$ LANGUAGE plpgsql VOLATILE;

                     CREATE OR REPLACE FUNCTION %1$s.tabs_event() RETURNS TRIGGER AS $TRG5$
                     BEGIN
                        PERFORM public.add_tracked_event(%2$s, 0, NEW.id, ''SPARK_Custom_Tab_Created'');
                        
                        RETURN NEW;
                     END;
                     $TRG5$ LANGUAGE plpgsql VOLATILE;

                     CREATE OR REPLACE FUNCTION %1$s.contact_accounts_event() RETURNS TRIGGER AS $TRG6$
                     BEGIN
                        SET search_path TO %1$s, public, targetwatch;

                        IF (NEW.source = listval(''contact_accounts source'', ''Health Market Science'', false)) THEN
                           PERFORM public.add_tracked_event(%2$s, NEW.users_id, NEW.id, ''SPARK_Account_Assigned_from_TargetWatch'');
                        ELSIF (NEW.source = listval(''contact_accounts source'', ''Referral Source Database'', false)) THEN
                           PERFORM public.add_tracked_event(%2$s, NEW.users_id, NEW.id, ''SPARK_Account_Assigned_from_RSDB'');
                        END IF;

                        RETURN NEW;
                     END;
                     $TRG6$ LANGUAGE plpgsql VOLATILE;

                     CREATE OR REPLACE FUNCTION %1$s.contacts_event() RETURNS TRIGGER AS $TRG7$
                     BEGIN
                        SET search_path TO %1$s, public, targetwatch;

                        IF (NEW.source = listval(''contacts source'', ''Health Market Science'', false)) THEN
                           PERFORM public.add_tracked_event(%2$s, NEW.users_id, NEW.id, ''SPARK_Contact_Assigned_from_TargetWatch'');
                        ELSIF (NEW.source = listval(''contacts source'', ''Referral Source Database'', false)) THEN
                           PERFORM public.add_tracked_event(%2$s, NEW.users_id, NEW.id, ''SPARK_Contact_Assigned_from_RSDB'');
                        END IF;

                        RETURN NEW;
                     END;
                     $TRG7$ LANGUAGE plpgsql VOLATILE;

                     CREATE OR REPLACE FUNCTION %1$s.territory_event() RETURNS TRIGGER AS $TRG8$
                     BEGIN
                        PERFORM public.add_tracked_event(%2$s, 0, NEW.id, ''SPARK_Sales_Territory_Created'');

                        RETURN NEW;
                     END;
                     $TRG8$ LANGUAGE plpgsql VOLATILE;
                    ', r.db_id, r.id);
      RAISE NOTICE '\tAdding Misc Client Events...';
      EXECUTE cmd;

   END LOOP;
   
END;
$$ LANGUAGE plpgsql;


DO $$
DECLARE
   r record;
   cmd text;
BEGIN
   FOR r IN SELECT id, db_id FROM public.accounts WHERE status = 2
   LOOP
      RAISE NOTICE 'Processing Account Id: %', r.id;
   
      IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = r.db_id AND table_name = 'change_log') THEN  
         cmd := FORMAT('DROP TRIGGER IF EXISTS change_log_instr ON %1$s.change_log;
                        CREATE TRIGGER change_log_instr AFTER INSERT ON %1$s.change_log
                        FOR EACH ROW
                            EXECUTE PROCEDURE %1$s.change_log_event();', r.db_id, r.id);
         RAISE NOTICE '\tAdding change_log trigger...';
         EXECUTE cmd;
      ELSE
         RAISE NOTICE '\t\tWARNING: Events based on change_log will not be tracked for this client because the table does not exist!';
      END IF;

      IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = r.db_id AND table_name = 'applog') THEN  
         cmd := FORMAT('DROP TRIGGER IF EXISTS applog_instr ON %1$s.applog;
                        CREATE TRIGGER applog_instr AFTER INSERT ON %1$s.applog
                        FOR EACH ROW
                            EXECUTE PROCEDURE %1$s.applog_event();', r.db_id, r.id);
         RAISE NOTICE '\tAdding applog trigger...';
         EXECUTE cmd;
      ELSE
         RAISE NOTICE '\t\tWARNING: Events based on applog will not be tracked for this client because the table does not exist!';
      END IF;

      IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = r.db_id AND table_name = 'tw_target') THEN  
         cmd := FORMAT('DROP TRIGGER IF EXISTS tw_target_instr ON %1$s.tw_target;
                        CREATE TRIGGER tw_target_instr AFTER INSERT ON %1$s.tw_target
                        FOR EACH ROW
                            EXECUTE PROCEDURE %1$s.tw_target_event();', r.db_id, r.id);
         RAISE NOTICE '\tAdding tw_target trigger...';
         EXECUTE cmd;
      ELSE
         RAISE NOTICE '\t\tWARNING: Events based on tw_target will not be tracked for this client because the table does not exist!';
      END IF;
             
      IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = r.db_id AND table_name = 'object_list') THEN  
         cmd := FORMAT('DROP TRIGGER IF EXISTS object_list_instr ON %1$s.object_list;
                        CREATE TRIGGER object_list_instr AFTER INSERT ON %1$s.object_list
                        FOR EACH ROW
                            EXECUTE PROCEDURE %1$s.object_list_event();', r.db_id, r.id);
         RAISE NOTICE '\tAdding object_list trigger...';
         EXECUTE cmd;
      ELSE
         RAISE NOTICE '\t\tWARNING: Events based on object_list will not be tracked for this client because the table does not exist!';
      END IF;
             
      IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = r.db_id AND table_name = 'tabs') THEN  
         cmd := FORMAT('DROP TRIGGER IF EXISTS tabs_instr ON %1$s.tabs;
                        CREATE TRIGGER tabs_instr AFTER INSERT ON %1$s.tabs
                        FOR EACH ROW
                            EXECUTE PROCEDURE %1$s.tabs_event();', r.db_id, r.id);
         RAISE NOTICE '\tAdding tabs trigger...';
         EXECUTE cmd;
      ELSE
         RAISE NOTICE '\t\tWARNING: Events based on tabs will not be tracked for this client because the table does not exist!';
      END IF;
   
      IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = r.db_id AND table_name = 'contact_accounts') THEN  
         cmd := FORMAT('DROP TRIGGER IF EXISTS contact_accounts_instr ON %1$s.contact_accounts;
                        CREATE TRIGGER contact_accounts_instr AFTER INSERT ON %1$s.contact_accounts
                        FOR EACH ROW
                            EXECUTE PROCEDURE %1$s.contact_accounts_event();', r.db_id, r.id);
         RAISE NOTICE '\tAdding contact_accounts trigger...';
         EXECUTE cmd;
      ELSE
         RAISE NOTICE '\t\tWARNING: Events based on contact_accounts will not be tracked for this client because the table does not exist!';
      END IF;
   
      IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = r.db_id AND table_name = 'contacts') THEN  
         cmd := FORMAT('DROP TRIGGER IF EXISTS contacts_instr ON %1$s.contacts;
                        CREATE TRIGGER contacts_instr AFTER INSERT ON %1$s.contacts
                        FOR EACH ROW
                            EXECUTE PROCEDURE %1$s.contacts_event();', r.db_id, r.id);
         RAISE NOTICE '\tAdding contacts trigger...';
         EXECUTE cmd;
      ELSE
         RAISE NOTICE '\t\tWARNING: Events based on contacts will not be tracked for this client because the table does not exist!';
      END IF;
   
      IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = r.db_id AND table_name = 'territory') THEN  
         cmd := FORMAT('DROP TRIGGER IF EXISTS territory_instr ON %1$s.territory;
                        CREATE TRIGGER territory_instr AFTER INSERT ON %1$s.territory
                        FOR EACH ROW
                            EXECUTE PROCEDURE %1$s.territory_event();', r.db_id, r.id);
         RAISE NOTICE '\tAdding territory trigger...';
         EXECUTE cmd;
      ELSE
         RAISE NOTICE '\t\tWARNING: Events based on territory will not be tracked for this client because the table does not exist!';
      END IF;
   
   END LOOP;
   
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION public.accounts_event()
RETURNS TRIGGER AS $$
DECLARE
   cmd TEXT;
BEGIN

   cmd := FORMAT('CREATE OR REPLACE FUNCTION %1$s.change_log_event() RETURNS TRIGGER AS $TRG1$
                  BEGIN
                     SET search_path TO %1$s, public, targetwatch;

                     -- Add actions
                     IF NEW.action = 1 THEN
                        IF NEW.object_list_id = objectid(''expenses'') THEN
                           PERFORM public.add_tracked_event(%2$s, NEW.users_id, NEW.action_id, ''SPARK_Expense_Added'');
                        ELSIF NEW.object_list_id = objectid(''events'') THEN
                           PERFORM public.add_tracked_event(%2$s, NEW.users_id, NEW.action_id, ''SPARK_Event_Created'');
                        ELSIF NEW.object_list_id = objectid(''events_series'') THEN
                           PERFORM public.add_tracked_event(%2$s, NEW.users_id, NEW.action_id, ''SPARK_Recurring_Event_Series_Created'');
                        END IF;
                     -- Update Actions
                     ELSIF NEW.action = 2 THEN
                        IF NEW.object_list_id = objectid(''events'') AND NEW.notes = ''Set complete: Completed'' THEN
                           PERFORM public.add_tracked_event(%2$s, NEW.users_id, NEW.action_id, ''SPARK_Event_Completed'');
                        END IF;
                     -- Delete Actions
                     ELSIF NEW.action = 3 THEN
                        IF NEW.object_list_id = objectid(''territory'') THEN
                           PERFORM public.add_tracked_event(%2$s, NEW.users_id, NEW.action_id, ''SPARK_Sales_Territory_Removed'');
                        END IF;
                     END IF;
                     
                     RETURN NEW;
                  END;
                  $TRG1$ LANGUAGE plpgsql VOLATILE;', NEW.db_id, NEW.id, '%coachs_list%', '%"coachs_list_id":null%');
   RAISE NOTICE '\tAdding Change Log Events...';
   EXECUTE cmd;

   cmd := FORMAT('CREATE OR REPLACE FUNCTION %1$s.applog_event() RETURNS TRIGGER AS $TRG2$
                  DECLARE
                     ids BIGINT[];
                  BEGIN
                     SET search_path TO %1$s, public, targetwatch;
                     
                     -- View actions
                     IF NEW.uri ILIKE %3$L THEN
                        PERFORM public.add_tracked_event(%2$s, NEW.users_id, NEW.id, ''SPARK_Request_for_Account'');
                     ELSIF NEW.uri ILIKE %4$L THEN
                        PERFORM public.add_tracked_event(%2$s, NEW.users_id, NEW.id, ''SPARK_Request_for_Contact'');
                     ELSIF NEW.uri ILIKE %5$L THEN
                        PERFORM public.add_tracked_event(%2$s, NEW.users_id, NEW.id, ''SPARK_Request_for_Referral'');
                     ELSIF NEW.uri ILIKE %6$L THEN
                        SELECT STRING_TO_ARRAY(REPLACE(SUBSTRING(uri, POSITION(''ids'' in uri)+4, POSITION(''redirect'' in uri) - (POSITION(''ids'' in uri)+5)), ''/'',''''), '','')::BIGINT[] INTO ids  FROM %1$s.applog WHERE uri ILIKE %6$L;
                        IF EXISTS (SELECT 1 FROM %1$s.merged_records WHERE from_id = ANY(ARRAY[ids])) THEN
                           PERFORM public.add_tracked_event(%2$s, NEW.users_id, NEW.id, ''SPARK_Merge_Completed'');
                        END IF;
                     ELSIF NEW.uri ILIKE %7$L THEN
                        IF NEW.uri ILIKE %8$L AND NEW.uri NOT ILIKE %9$L THEN
                           PERFORM public.add_tracked_event(%2$s, NEW.users_id, NEW.id, ''SPARK_TargetWatch_Export'');
                        END IF;
                     END IF;
                     
                     RETURN NEW;
                  END;
                  $TRG2$ LANGUAGE plpgsql VOLATILE;', NEW.db_id, NEW.id, '/s3/contact_accounts%', '/s3/contacts%', '/s3/referrals%', '%merge%redirect%', '%export%', '/ajax/targetwatch%', '%delete%');
   RAISE NOTICE '\tAdding App Log Events...';
   EXECUTE cmd;


   cmd := FORMAT('CREATE OR REPLACE FUNCTION %1$s.tw_target_event() RETURNS TRIGGER AS $TRG3$
                  BEGIN
                     PERFORM public.add_tracked_event(%2$s, NEW.users_id, NEW.id, ''SPARK_Target_Created'');
                     
                     RETURN NEW;
                  END;
                  $TRG3$ LANGUAGE plpgsql VOLATILE;

                  CREATE OR REPLACE FUNCTION %1$s.object_list_event() RETURNS TRIGGER AS $TRG4$
                  BEGIN
                     PERFORM public.add_tracked_event(%2$s, 0, NEW.id, ''SPARK_Custom_Object_Created'');
                     
                     RETURN NEW;
                  END;
                  $TRG4$ LANGUAGE plpgsql VOLATILE;

                  CREATE OR REPLACE FUNCTION %1$s.tabs_event() RETURNS TRIGGER AS $TRG5$
                  BEGIN
                     PERFORM public.add_tracked_event(%2$s, 0, NEW.id, ''SPARK_Custom_Tab_Created'');
                     
                     RETURN NEW;
                  END;
                  $TRG5$ LANGUAGE plpgsql VOLATILE;

                  CREATE OR REPLACE FUNCTION %1$s.contact_accounts_event() RETURNS TRIGGER AS $TRG6$
                  BEGIN
                     SET search_path TO %1$s, public, targetwatch;

                     IF (NEW.source = listval(''contact_accounts source'', ''Health Market Science'', false)) THEN
                        PERFORM public.add_tracked_event(%2$s, NEW.users_id, NEW.id, ''SPARK_Account_Assigned_from_TargetWatch'');
                     ELSIF (NEW.source = listval(''contact_accounts source'', ''Referral Source Database'', false)) THEN
                        PERFORM public.add_tracked_event(%2$s, NEW.users_id, NEW.id, ''SPARK_Account_Assigned_from_RSDB'');
                     END IF;

                     RETURN NEW;
                  END;
                  $TRG6$ LANGUAGE plpgsql VOLATILE;

                  CREATE OR REPLACE FUNCTION %1$s.contacts_event() RETURNS TRIGGER AS $TRG7$
                  BEGIN
                     SET search_path TO %1$s, public, targetwatch;

                     IF (NEW.source = listval(''contacts source'', ''Health Market Science'', false)) THEN
                        PERFORM public.add_tracked_event(%2$s, NEW.users_id, NEW.id, ''SPARK_Contact_Assigned_from_TargetWatch'');
                     ELSIF (NEW.source = listval(''contacts source'', ''Referral Source Database'', false)) THEN
                        PERFORM public.add_tracked_event(%2$s, NEW.users_id, NEW.id, ''SPARK_Contact_Assigned_from_RSDB'');
                     END IF;

                     RETURN NEW;
                  END;
                  $TRG7$ LANGUAGE plpgsql VOLATILE;

                  CREATE OR REPLACE FUNCTION %1$s.territory_event() RETURNS TRIGGER AS $TRG8$
                  BEGIN
                     PERFORM public.add_tracked_event(%2$s, 0, NEW.id, ''SPARK_Sales_Territory_Created'');

                     RETURN NEW;
                  END;
                  $TRG8$ LANGUAGE plpgsql VOLATILE;
                 ', NEW.db_id, NEW.id);
   RAISE NOTICE '\tAdding Misc Client Events...';
   EXECUTE cmd;

   IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = NEW.db_id AND table_name = 'change_log') THEN  
      cmd := FORMAT('DROP TRIGGER IF EXISTS change_log_instr ON %1$s.change_log;
                     CREATE TRIGGER change_log_instr AFTER INSERT ON %1$s.change_log
                     FOR EACH ROW
                         EXECUTE PROCEDURE %1$s.change_log_event();', NEW.db_id, NEW.id);
      RAISE NOTICE '\tAdding change_log trigger...';
      EXECUTE cmd;
   ELSE
      RAISE NOTICE '\t\tWARNING: Events based on change_log will not be tracked for this client because the table does not exist!';
   END IF;

   IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = NEW.db_id AND table_name = 'applog') THEN  
      cmd := FORMAT('DROP TRIGGER IF EXISTS applog_instr ON %1$s.applog;
                     CREATE TRIGGER applog_instr AFTER INSERT ON %1$s.applog
                     FOR EACH ROW
                         EXECUTE PROCEDURE %1$s.applog_event();', NEW.db_id, NEW.id);
      RAISE NOTICE '\tAdding applog trigger...';
      EXECUTE cmd;
   ELSE
      RAISE NOTICE '\t\tWARNING: Events based on applog will not be tracked for this client because the table does not exist!';
   END IF;

   IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = NEW.db_id AND table_name = 'tw_target') THEN  
      cmd := FORMAT('DROP TRIGGER IF EXISTS tw_target_instr ON %1$s.tw_target;
                     CREATE TRIGGER tw_target_instr AFTER INSERT ON %1$s.tw_target
                     FOR EACH ROW
                         EXECUTE PROCEDURE %1$s.tw_target_event();', NEW.db_id, NEW.id);
      RAISE NOTICE '\tAdding tw_target trigger...';
      EXECUTE cmd;
   ELSE
      RAISE NOTICE '\t\tWARNING: Events based on tw_target will not be tracked for this client because the table does not exist!';
   END IF;
          
   IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = NEW.db_id AND table_name = 'object_list') THEN  
      cmd := FORMAT('DROP TRIGGER IF EXISTS object_list_instr ON %1$s.object_list;
                     CREATE TRIGGER object_list_instr AFTER INSERT ON %1$s.object_list
                     FOR EACH ROW
                         EXECUTE PROCEDURE %1$s.object_list_event();', NEW.db_id, NEW.id);
      RAISE NOTICE '\tAdding object_list trigger...';
      EXECUTE cmd;
   ELSE
      RAISE NOTICE '\t\tWARNING: Events based on object_list will not be tracked for this client because the table does not exist!';
   END IF;
          
   IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = NEW.db_id AND table_name = 'tabs') THEN  
      cmd := FORMAT('DROP TRIGGER IF EXISTS tabs_instr ON %1$s.tabs;
                     CREATE TRIGGER tabs_instr AFTER INSERT ON %1$s.tabs
                     FOR EACH ROW
                         EXECUTE PROCEDURE %1$s.tabs_event();', NEW.db_id, NEW.id);
      RAISE NOTICE '\tAdding tabs trigger...';
      EXECUTE cmd;
   ELSE
      RAISE NOTICE '\t\tWARNING: Events based on tabs will not be tracked for this client because the table does not exist!';
   END IF;

   IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = NEW.db_id AND table_name = 'contact_accounts') THEN  
      cmd := FORMAT('DROP TRIGGER IF EXISTS contact_accounts_instr ON %1$s.contact_accounts;
                     CREATE TRIGGER contact_accounts_instr AFTER INSERT ON %1$s.contact_accounts
                     FOR EACH ROW
                         EXECUTE PROCEDURE %1$s.contact_accounts_event();', NEW.db_id, NEW.id);
      RAISE NOTICE '\tAdding contact_accounts trigger...';
      EXECUTE cmd;
   ELSE
      RAISE NOTICE '\t\tWARNING: Events based on contact_accounts will not be tracked for this client because the table does not exist!';
   END IF;

   IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = NEW.db_id AND table_name = 'contacts') THEN  
      cmd := FORMAT('DROP TRIGGER IF EXISTS contacts_instr ON %1$s.contacts;
                     CREATE TRIGGER contacts_instr AFTER INSERT ON %1$s.contacts
                     FOR EACH ROW
                         EXECUTE PROCEDURE %1$s.contacts_event();', NEW.db_id, NEW.id);
      RAISE NOTICE '\tAdding contacts trigger...';
      EXECUTE cmd;
   ELSE
      RAISE NOTICE '\t\tWARNING: Events based on contacts will not be tracked for this client because the table does not exist!';
   END IF;

   IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = NEW.db_id AND table_name = 'territory') THEN  
      cmd := FORMAT('DROP TRIGGER IF EXISTS territory_instr ON %1$s.territory;
                     CREATE TRIGGER territory_instr AFTER INSERT ON %1$s.territory
                     FOR EACH ROW
                         EXECUTE PROCEDURE %1$s.territory_event();', NEW.db_id, NEW.id);
      RAISE NOTICE '\tAdding territory trigger...';
      EXECUTE cmd;
   ELSE
      RAISE NOTICE '\t\tWARNING: Events based on territory will not be tracked for this client because the table does not exist!';
   END IF;
   
   RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER accounts_instr AFTER UPDATE ON public.accounts
FOR EACH ROW
   EXECUTE PROCEDURE public.accounts_event();